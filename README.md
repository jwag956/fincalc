**Fincalc**

A simple Python program that computes XIRR (of excel fame) given a csv file with required info.
This csv file can have any number of securities, and multiple 'test dates'.

The idea behind this tool was to answer the simple question: over time I buy and sell a particular
security, I reinvest dividends, and pay tax on those dividends. If I sold at a partiucular
date I would pay (long term) tax on the gain. What is my annual rate of return?
I couldn't find anything remotely close to this readily available.

Note that this is pretty much nothing to do with gain/loss from a tax perspective - in fact
it became clear once running this program that I could have capital losses but still be
making say a 3% return (due to reinvested dividends).
