# Copyright 2018 by J. Christopher Wagner (jwag). All rights reserved.

from datetime import timedelta
from typing import List


def dates_to_years_ago(dates) -> List[float]:
    """ Convert a list of dates to list of relative floats (first value 0) """
    days = [0]
    for i in range(len(dates) - 1):
        days.append((dates[i+1] - dates[0]) / timedelta(days=1))
    # print(days)
    return [d/365 for d in days]


def d2f(value: str) -> float:
    """
    Convert 'currency' to float.
    remove dollar signs and commas
    """
    if not value:
        return 0.0
    v = value.replace('$', '').replace(',', '')
    return float(v)
