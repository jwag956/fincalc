# Copyright 2018 by J. Christopher Wagner (jwag). All rights reserved.

import argparse
import datetime
from dateutil import parser
import numpy as np
from scipy.optimize import fsolve

import irrcsv
import utils

LT_RATE = 0.20
VTABX_CASH = np.array([-25000, -50000, -25000, -40000, -354, -487, -747, -20000, 171971])
VTABX_DATES = ("2/10/15", "2/12/15", "3/24/15", "8/31/15", "12/31/15", "12/31/16", "12/31/17", "1/28/18", "7/17/18")

VAPIX_CASH = np.array([-50000, -409, -652, -45451, -5285, -690, -822, -309, 503, -905, 109370])
VAPIX_DATES = ("7/7/11", "12/31/11", "12/31/12", "2/20/13", "7/24/13", "12/31/13", "12/31/14", "12/31/15", "12/31/16",
               "12/31/17", "7/17/18")


def npv(guess, cfs, yrs):
    return np.sum(cfs / (1. + guess) ** yrs)


def irr(cfs, yrs, x0):
    """ return percent IRR """
    return np.asscalar(fsolve(npv, x0=x0, args=(cfs, yrs))) * 100


def fetch_columns(csvfile, rows: tuple, column):
    """
    From a csv file and args - return 2 lists - one of dates and one of cash flows.
    Ignore rows with empty dates.
    """
    table = np.genfromtxt(csvfile, dtype=str, delimiter=',', invalid_raise=False, usecols=(int(column), int(column)+1),
                          loose=True)
    rows = table[rows[0]:rows[1]:1]
    dates = []
    values = []
    for x, y in rows:
        if x:
            dates.append(parser.parse(x))
            values.append(int(y))

    return dates, values


def compute_final(basis, shares, price, dt: datetime = None):
    """
    Compute final entries as if all assets sold on date given.
    Assume all gains are LT and taxed at 20%.
    If no date - assume today.

    Returns 2 lists - dates and values
    First element is tax, second is income from sale.
    """
    if not dt:
        dt = datetime.datetime.now()
    income = shares * price
    gain = income - basis
    tax = gain * LT_RATE

    return [dt, dt], [int(-tax) if tax > 0 else 0, int(income)]


def parseargs():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--irrcsv", help='Path to csv file generated from irr - used for batch')
    arg_parser.add_argument("--csv", help='Path to csv file')
    arg_parser.add_argument("--rows", help='Rows (start,end) inclusive')
    arg_parser.add_argument("--column", help='Column (0-based) for date - assume cash flows are next column')
    arg_parser.add_argument("--basis", type=float, help='Basis for shares given with --shares')
    arg_parser.add_argument("--shares", type=float, help='Total number of shares held')
    arg_parser.add_argument("--price", type=float, help='Current price per share')
    arg_parser.add_argument("--asof", help='Date of --basis, --price, --shares')
    arg_parser.add_argument("--verbose", "-v", action='count')
    return arg_parser.parse_args()


if __name__ == '__main__':

    args = parseargs()

    # years = np.array(dates_to_years_ago([parser.parse(d) for d in VTABX_DATES]))
    # print("VTABX", round(irr(VTABX_CASH, years, 0.20), 3))
    # print("VAPIX", round(irr(VAPIX_CASH, np.array(dates_to_years_ago(VAPIX_DATES)), 0.20), 3))

    holdings = {}
    if args.csv:
        # convert rows arg into int tuple
        rownums = (int(args.rows.split(',')[0]) - 1, int(args.rows.split(',')[1]))
        (d, v) = fetch_columns(args.csv, rownums, args.column)
        # If provided a basis, shares, price - compute 'final' row entries
        if args.basis:
            fd, fv = compute_final(args.basis, args.shares, args.price, args.asof)
            d.extend(fd)
            v.extend(fv)
        holdings = {'CLI': [{
            'dates': d,
            'values': v,
            'asof': parser.parse(args.asof),
            'basis': utils.d2f(args.basis),
            'price': utils.d2f(args.price),
            'shares': float(args.shares)
        }]}
    elif args.irrcsv:
        holdings = irrcsv.parse_file(args.irrcsv)
    else:
        print("Must specify --irrcsv or --csv")
        exit(1)

    for symbol, holding in holdings.items():
        for pit in holding:
            d = pit['dates']
            v = pit['values']
            # compute final value based on price, shares etc.
            if pit.get('basis', None):
                fd, fv = compute_final(pit['basis'], pit['shares'], pit['price'], pit['asof'])
                d.extend(fd)
                v.extend(fv)

            yago = utils.dates_to_years_ago(d)
            if args.verbose:
                print(symbol)
                print("\tDates: ", [pd.strftime('%Y-%m-%d') for pd in d])
                print("\tValues: ", v)
                if args.verbose > 1:
                    print("\tYrs Ago: ", yago)
                print("\tShares: ", pit.get('shares', 'N/A'), "Price: ", pit.get('price', 'N/A'))

            print('{sym} {date} IRR {val}%  Market Value ${mv} Since {since}'.format(
                sym=symbol,
                date=pit.get('asof').strftime('%Y-%m-%d'),
                val=round(irr(np.array(v), np.array(yago), 0.20), 3),
                mv=round(pit.get('shares', 0) * pit.get('price', 0), 0),
                since=sorted(d)[0].strftime('%Y')))
