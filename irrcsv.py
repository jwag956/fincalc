# Copyright 2018 by J. Christopher Wagner (jwag). All rights reserved.

import csv
from dateutil import parser

import utils

"""
Parse the irr csv to get:
dates/values of purchases, dividends etc
cost basis, shares, price

return a dict:

{ <symbol>: [{
    dates: [],
    values: [],
    shares:
    basis:
    price:
    asof: <date>
},
...]
}
"""


def parse_file(filename):
    results = {}
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row.get('Symbol', None):
                # new section
                results[row['Symbol']] = process_symbol(reader)
    return results


def process_symbol(reader):
    """
    Read rows and build date, value information.
    There might be more than one set of basis etc info.
    """
    rv = []
    accumulate_info = True
    dates = []
    values = []
    summaries = []
    for row in reader:
        # First empty date - we stop accumulating
        # First empty row we return.
        if all(not v for k, v in row.items()):
            break
        if row['Date'] and accumulate_info:
            dates.append(parser.parse(row['Date']))
            values.append(utils.d2f(row['Buy/Sell/Tax']))
        else:
            accumulate_info = False
        # Look for row with shares/basis info
        if all(v for v in [row.get(k, None) for k in ['Basis', 'Shares', 'Price']]):
            summaries.append({
                'shares': float(row.get('Shares')),
                'basis': utils.d2f(row.get('Basis')),
                'price': utils.d2f(row.get('Price')),
                'asof': parser.parse(row.get('Date'))
            })
    # For each summary line - collect dates and values LE than date of summary
    for summary in summaries:
        pit = {}
        pit.update(summary)
        pit['dates'] = [d for d in dates if d <= summary['asof']]
        pit['values'] = values[0:len(pit['dates'])]
        rv.append(pit)
    return rv



